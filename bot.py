import requests
from web3 import Web3, WebsocketProvider
import json
from telebot import TeleBot
from telebot.types import Message
from threading import Thread
web3 = Web3(WebsocketProvider('wss://data-seed-prebsc-1-s3.binance.org:8545/'))

contract_address = '0xe62C422c1E8083CE3b4526Ff0b16388354AB6E64'
wbnb_address = '0xae13d989daC2f0dEbFf460aC112a837C89BAa7cd'
cake_address = '0x8d008B313C1d6C7fE2982F62d32Da7507cF43551'
router_address = '0xD99D1c33F9fC3444f8101754aBC46c52416550D1'

with open('abi/bep20.json') as bep20_abi_file:
   bep20_abi = json.load(bep20_abi_file)
wbnb_contract = web3.eth.contract(address = wbnb_address, abi = bep20_abi)
cake_contract = web3.eth.contract(address = cake_address, abi = bep20_abi)

with open('abi/contract.json') as contract_abi_file:
    contract_abi = json.load(contract_abi_file)
contract = web3.eth.contract(address = contract_address, abi = contract_abi)

with open('abi/router.json') as router_abi_file:
    router_abi = json.load(router_abi_file)
router_contract = web3.eth.contract(address = router_address, abi = router_abi)

# wbnb_Transfer_event_filter = wbnb_contract.events.Transfer.create_filter(fromBlock='latest')
# cake_Transfer_event_filter = cake_contract.events.Transfer.create_filter(fromBlock='latest')
contract_Swap_event_filter = contract.events.Swap.create_filter(fromBlock='latest')

def format_number(number: int | float, round_number: int = 0):
    if (round_number == 0):
        return format(int(round(number)),',')
    else:
        return format(round(number,round_number),',')

# Define a callback function that will be called whenever a new Transfer event is detected
def handle_event(event):
    print(f"New Transfer event detected! From: {event['args']['from']}, To: {event['args']['to']}, Value: {event['args']['value']}")

# Start listening for new Transfer events

def main():
    print('Bot is running.')
    bot = TeleBot('6245021446:AAGLnnZr3fRDTheiEjzQ1mB1gpvIuepGSGo')
    interacting_chat_ids = []
    @bot.message_handler(commands=['start'])
    def send_welcome(message: Message):
        try:
            interacting_chat_ids.index(message.chat.id)
        except ValueError:
            pass
        finally:    
            interacting_chat_ids.append(message.chat.id)
            bot.reply_to(message, "Auto-updated newest transactions is being activated.")
    def send_message(caption):
        for chat_id in interacting_chat_ids:
            photo_path = open('asset/pancakeswap.png', 'br')   
            bot.send_photo(chat_id=chat_id,
                           photo=photo_path,
                           caption=caption)
    def auto_send_messages():
        while True:
    # for event in wbnb_Transfer_event_filter.get_new_entries():
    #     sender = event['args']['from']
    #     recipient = event['args']['to']
    #     amount = event['args']['value']
    #     if (recipient == contract_address):    
    #         handle_event(event)
    #         print(contract_wbnb_balance)
    #         print(contract_cake_balance)
            try:
                for event in contract_Swap_event_filter.get_new_entries():
            
                    in_wei = pow(10,18)
                    recipient = event['args']['recipient']
                    pay_wbnb = int(event['args']['amount1'])/in_wei
                    receive_cake = abs(int(event['args']['amount0']))/in_wei
                    tx_hash = hex(int.from_bytes(event['transactionHash']))
                        
                    contract_wbnb_balance = wbnb_contract.functions.balanceOf(contract_address).call()
                    recipient_cake_balance = cake_contract.functions.balanceOf(recipient).call()

                    coin_symbol = 'WBNB'
                    currency_symbol = 'USD'
                        
                    api_url = f'https://api.coingecko.com/api/v3/simple/price?ids={coin_symbol}&vs_currencies={currency_symbol}'
                    response = requests.get(api_url)
                    data = response.json()

                    # Extract the price from the response data
                    wbnb_price = data[coin_symbol.lower()][currency_symbol.lower()]
                    transaction_worth = wbnb_price*pay_wbnb
                    total_supply = cake_contract.functions.totalSupply().call()/in_wei
                    trade_ratio = pay_wbnb/receive_cake
                    market_cap = total_supply*(wbnb_price*trade_ratio)
                    position = (receive_cake*in_wei*100)/(recipient_cake_balance - receive_cake*in_wei)
                    telegram_caption = f'''💴 Pay: {format_number(pay_wbnb,3)} WBNB (${format_number(transaction_worth,3)})
🥮 Receive: {format_number(receive_cake,3)} Cake
👩🏼‍💼 Recipient: https://testnet.bscscan.com/address/{recipient}
#️⃣ Transaction Hash: https://testnet.bscscan.com/tx/{tx_hash}
📈 Position: +{format_number(position,3)}%
🏛  Market Cap: ${format_number(market_cap)}''' 
                    print(telegram_caption)
                    send_message(telegram_caption)
            except ValueError:
                print('The filter cannot be found.')
    def bot_polling():
        bot.polling()
    thread1 = Thread(target=bot_polling)
    thread2 = Thread(target=auto_send_messages)
    
    thread1.start()
    thread2.start()

    # wait for both threads to finish
    thread1.join()
    thread2.join()


if __name__ == '__main__':
    main()

        